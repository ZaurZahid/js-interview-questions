// This is a demo task.
// Write a function:

// function solution(A);

// that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.

// For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.

// Given A = [1, 2, 3], the function should return 4.

// Given A = [−1, −3], the function should return 1.

// Write an efficient algorithm for the following assumptions:

// N is an integer within the range [1..100,000];
// each element of array A is an integer within the range [−1,000,000..1,000,000].


const solution = (A) => {
    let arr = A.filter(el => el >= 1).sort((a, b) => a - b)
    let smallest = 1
    console.log(arr)
    for (let i = 0; i < arr.length; i++) {
        let element = arr[i]

        if (smallest < element) {
            return smallest
        }
        smallest = element + 1
    }
    return smallest
}

console.log(solution([1, 3, 6, 4, 1, 2]))


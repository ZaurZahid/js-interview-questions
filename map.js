// creating map function from scratch
const map = (arr, cb) => {
    let newArr = []
    for (let i = 0; i < arr.length; i++) {
        const element = arr[i];
        newArr = [...newArr, cb(element)]
    }

    return newArr
}

function double(i) { return i * 2; }

const output = map([1, 2, 3], double)
console.log(output) //[2,4,6]
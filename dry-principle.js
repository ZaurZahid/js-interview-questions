/* Don't Repeat Yourself
Reducing the repetition of patterns and code duplication in favor of abstractions and avoiding redundancy. */
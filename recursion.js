// What Is Recursion?
// In the most basic of terms, recursion is when a function keeps calling itself until it doesn't have to anymore.

// What? Yeah, the function keeps calling itself but with a smaller input every single time.

// Think of recursion as a circuit race. It's like running the same track over and over again but the laps keep getting smaller every time. Eventually, you're going to run the last, smallest lap and the race will be over.

// Same with recursion: the function keeps calling itself with smaller input and eventually it stops.

// But, the function doesn't decide for itself when to stop. We tell it when to stop. We give the function a condition known as a base case.

// The base case is the condition that tells the function when to stop calling itself. It like telling the function what the last lap in the race will be so it stops running after that lap.

// Examples of Recursion
// Alright that's recursion. Let's look at some examples to understand how recursion works.

// Remember the first time you learned about loops? The first example you probably did was a countdown program. Let's do that.

// First, let's understand what we want our program to do. Count down from a given number to the smallest number, subtracting 1 every time.

// Given the number 5, we expect the output to be something like:

//  5
//  4
//  3
//  2
//  1
// Alright, how can we code this program with recursion?

// let countDown = number => {
//     if (number === 0) {
//         return;
//     }
//     console.log(number);
//     return countDown(number - 1);
// };
// console.log(countDown(5)) // 5, 4, 3, 2, 1



//Fibonacci number
// function fib(n) {
//     if (n < 2) {
//         return n;
//     }
//     return fib(n - 1) + fib(n - 2); // Fn-1 + Fn-2
// }

// console.log(fib(8)); // 21

//Optimizing recursive Fibonacci
function fib(n, memo) {
    if (n < 2) {
        return n;
    }
    if (!memo[n]) {
        memo[n] = fib(n - 1, memo) + fib(n - 2, memo);
    }
    return memo[n];
}

console.log(fib(8, {})); 
// Implement Function.prototype.bind()

/*
    const foo = function() {
        console.log(this.bar);
    }
    let baz = foo.bind({bar: 'hello'})
    baz(); // Hello
*/


Function.prototype.bind = function (context) {
    const _this = this;

    return function () {
        _this.apply(context);
    }
}

const foo = function () {
    console.log(this.bar);
}
let baz = foo.bind({ bar: 'hello' })
baz()

/* bind, apply, and call are all methods that can be used to manipulate the this keyword in JavaScript. They are used to specify the value of this when invoking a function, and they differ in the way they pass arguments to the function.

Here are the differences between bind, apply, and call:
bind: creates a new function that has this set to a specific value, but it does not invoke the function immediately. Instead, it returns a new function that you can call later with the arguments you specify.

const person = {
  name: "John",
  age: 30,
  greet: function() {
    console.log(`Hello, my name is ${this.name} and I'm ${this.age} years old.`);
  }
};

const greet = person.greet.bind(person);
greet(); // Hello, my name is John and I'm 30 years old.

-----------------------------------------------------------------------------------------------------------

apply: invokes a function with this set to a specific value and with an array of arguments passed as the second parameter. The first parameter is the this value, and the second parameter is an array of arguments.

const numbers = [1, 2, 3, 4, 5];
const sum = function() {
  return this.reduce((acc, num) => acc + num);
}

const total = sum.apply(numbers);
console.log(total); // 15

-----------------------------------------------------------------------------------------------------------

call: invokes a function with this set to a specific value and with arguments passed individually. The first parameter is the this value, and the subsequent parameters are individual arguments.

const person = {
  name: "John",
  age: 30,
  greet: function(city) {
    console.log(`Hello, my name is ${this.name} and I'm ${this.age} years old. I live in ${city}.`);
  }
};

person.greet.call(person, 'London'); // Hello, my name is John and I'm 30 years old. I live in London.
*/
// How to count a number of words in given string in JavaScript ?

function WordCount(str) {
    // return str.split(' ')
    //     .filter(function (n) { return n != '' })
    //     .length;

    return str.trim().split(/\s+/).length;
}

console.log(WordCount("hello world a Zaur"));

function one(cb) {
    return cb ? cb(1) : 1;
}

function five(cb) {
    return cb ? cb(5) : 5;
}

function plus(n1) {
    // console.log(n1)
    return function (n2) { return n1 + n2; }
}

const result = one(plus(five(plus(one()))));   //  result = 7 (1 + 5 + 1)
console.log(result)
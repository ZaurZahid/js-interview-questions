function foo(x, array) {
    return array.includes(x) || array.some((item, i) => array.slice(i + 1).includes(x - item))
}

// tests:
console.log(foo(5, [2, 5, 6, 4, 3, 1])); // true
console.log(foo(5, [2, 6, 4, 3, 1])); // true
console.log(foo(10, [2, 5, 4, 3, 1])); // false
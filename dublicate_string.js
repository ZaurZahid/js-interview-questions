// Create a function that takes a string and returns a
// new string with duplicates removed


//const str = 'This is is a test test string';
// removeDuplicates(str)

const removeDuplicates = (str) => {
    const newArr = str.split(' ')
    const newSet = [...new Set(newArr)]

    return newSet.join(' ')
}

const str = 'This is is a test test string';
console.log(removeDuplicates(str)); // 'This is a test string'

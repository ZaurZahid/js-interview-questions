
//increment button value
import cx from 'classnames';
import { Component } from 'react';

export default class Counter extends Component {
    state = {
        count: 42
    }

    handleIncrement = () => {
        this.setState((prev, { count }) => ({
            count: prev.count + 1
        }))
    }

    render() {
        return (
            <>
                <div>
                    <h2>Counter</h2>
                    <h2 className={cx("counter")}>{this.state.count}</h2>
                    <button className={cx("counter-button")} onClick={this.handleIncrement}>Click</button>
                </div>
                <style>{`
                     .counter-button {
                         font-size: 1rem;
                         padding: 5px 10px;
                         color:  #585858;
                     }
                 `}</style>
            </>
        );
    }
}
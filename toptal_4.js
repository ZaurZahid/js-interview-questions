import cx from 'classnames';
import { Component } from 'react';

class TodoList extends Component {
    state = {
        items: []
    }

    handleSubmit = (e) => {
        e.preventDefault()

        if (this.input.value !== '') {
            const newItem = {
                text: this.input.value,
                isDone: false,
                key: Date.now()
            }
            this.setState((prev) => {
                return {
                    items: [...prev.items, newItem]
                }
            })

            this.input.value = ""
        }
    }

    handleProgress = (key) => {
        const changedData = this.state.items.map(item => item.key === key ? { ...item, isDone: !item.isDone } : item)

        this.setState(() => {
            return {
                items: changedData
            }
        })
    }

    render() {
        console.log(this.state.items)

        return (
            <>
                <div>
                    <h2>
                        Todo List
                    </h2>

                    <form onSubmit={this.handleSubmit}>
                        <input ref={(e) => this.input = e} />
                        <button type="submit">Add</button>
                    </form>

                    <p className={cx('task-counter')}>{this.state.items.filter(el => !el.isDone).length} remaining out of {this.state.items.length} tasks</p>

                    <ul>
                        {this.state.items.map((item) =>
                            <li key={item.key} className={cx(item.isDone ? 'is-done' : '')} onClick={() => this.handleProgress(item.key)}>{item.text}</li>
                        )}
                    </ul>
                </div>


                <style>{`
                     .is-done {
                         text-decoration: line-through;
                     }
                     .task-counter{
                         margin-bottom:20px
                     }
                 `}</style>
            </>
        );
    }
}

export default TodoList
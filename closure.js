// Closure
//https://medium.com/@prashantramnyc/javascript-closures-simplified-d0d23fa06ba4
function outer() {
    let b = 10;
    let c = 100;

    function inner() {
        let a = 20;
        console.log("a= " + a + " b= " + b);
        a++;
        b++;
    }
    return inner;
}

const X = outer();
const Y = outer();

X();  // a=20  b=10
X();  // a=20  b=11
X();  // a=20  b=12

Y();  // a=20  b=10

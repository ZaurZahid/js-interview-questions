// Symbol and Proxy
/* The Proxy object enables you to create a proxy for another object, 
which can intercept and redefine fundamental operations for that object. */

const target = {
    message1: "hello",
    message2: "everyone",
};

const handler1 = {};
const proxy1 = new Proxy(target, handler1);

console.log(proxy1.message1); // hello
console.log(proxy1.message2); // everyone


/* Symbol is a built-in object whose constructor returns a symbol primitive 
— also called a Symbol value or just a Symbol — that's guaranteed to be unique.  */
const sym1 = Symbol();
const sym2 = Symbol("foo");

/* guaranteed to be unique.  */
Symbol("foo") === Symbol("foo"); // false
const sym = new Symbol(); // TypeError

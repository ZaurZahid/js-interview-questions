// const myFunc = () => {
//     return new Promise((resolve, reject) => {
//         resolve("Heyyyyy")
//     })
// }

// myFunc().then(res => console.log(res))





// Create a sleep function that takes one parameter (time) and
// will wait "time" ms

/*
    async function run() {
        await sleep(500);
        console.log('hello');
        await sleep(500);
        console.log('world');
    }
*/

const sleep = (time) => new Promise((res, rej) => {
    setTimeout(() => {
        res()
    }, time);
})

async function run() {
    await sleep(500);
    console.log('hello');
    await sleep(2000);
    console.log('world');
}
run()
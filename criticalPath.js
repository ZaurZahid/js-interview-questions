/* The Critical Rendering Path is the sequence of steps
the browser goes through to convert
the HTML, CSS, and JavaScript into pixels on the screen */

// https://developer.mozilla.org/en-US/docs/Web/Performance/Critical_rendering_path#:~:text=The%20Critical%20Rendering%20Path%20is%20the%20sequence%20of,CSS%20Object%20Model%20%28CSSOM%29%2C%20render%20tree%20and%20layout.
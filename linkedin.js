function findAllHobbyists(hobby, hobbies) {
    let arr = []
    for (const [key, value] of Object.entries(hobbies)) {
        if (value.find(el => el === hobby)) {
            arr.push(key)
        }
    }
    return arr

}

var hobbies = {
    "Steve": ['Fashion', 'Piano', 'Reading'],
    "Patty": ['Drama', 'Magic', 'Pets'],
    "Chad": ['Puzzles', 'Pets', 'Yoga']
};

console.log(findAllHobbyists('Yoga', hobbies)); //Chad
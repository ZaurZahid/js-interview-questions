// You are given width (number), height (number), and symbol (string), output to the console a rectangle

// width: 5, height 4, symbol: *
// *****
// *****
// *****
// *****
// const createRectangle = (width, height, symbol) => {
//     let rec = ''

//     for (let i = 0; i < height; i++) {
//         for (let j = 0; j < width; j++) {
//             rec += symbol
//         }

//         if (i !== height - 1) rec += '\n'
//     }

//     console.log(rec)
// }
// createRectangle(5, 4, "*")





// width: 5, height 4, symbol: *
// *****
// *   *
// *   *
// *****

// const createRectangle = (width, height, symbol) => {
//     let rec = ''

//     for (let i = 0; i < height; i++) {
//         for (let j = 0; j < width; j++) {
//             if (j === 0 || j === width - 1 || i == 0 || i === height - 1) {
//                 rec += symbol
//             } else {
//                 rec += " "
//             }
//         }

//         if (i !== height - 1) rec += '\n'
//     }

//     console.log(rec)
// }

// createRectangle(5, 4, "*")

//      *
//     * *
//    * * *
//   * * * *
//  * * * * *

function createPyramid(rows) {
    for (let i = 0; i < rows; i++) {
        var output = '';
        for (let j = 0; j < rows - i; j++) output += ' ';
        for (let k = 0; k <= i; k++) output += '* ';
        console.log(output);
    }
}

createPyramid(5)


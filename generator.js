/* In JavaScript, a generator is a special type of function that can be paused and resumed later on, allowing for the generation of a sequence of values on demand. It is defined using the function* syntax, and uses the yield keyword to specify where the function should pause and return a value.
When a generator is called, it does not execute immediately like a regular function. Instead, it returns an iterator object that can be used to control the execution of the generator. Each time the next() method is called on the iterator, the generator resumes from where it left off and continues executing until it reaches the next yield statement.*/
function* numberGenerator() {
    let i = 0;
    while (true) {
      yield i++;
    }
}

const iterator = numberGenerator();
console.log(iterator.next().value); // 0
console.log(iterator.next().value); // 1
console.log(iterator.next().value); // 2

/* In this example, the numberGenerator function returns an iterator that generates an infinite sequence of numbers starting from 0. Each time the next() method is called on the iterator, the generator resumes from where it left off and returns the next number in the sequence using the yield keyword.
Generators are often used to generate sequences of data that are too large to be generated all at once, or to perform complex computations that can be broken down into smaller steps. They can also be used to implement asynchronous control flow using the yield keyword in combination with Promises or async/await syntax. */
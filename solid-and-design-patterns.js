// SOLID principles, design patterns, React 18
// https://hackernoon.com/understanding-solid-principles-in-javascript-w1cx3yrv

/* 
S represents the Single Responsibility principle   -   her funksiyanin oz isi olmalidir
O represents the Open Closed principle             -   Kecmisde yazilan kodlara el vurma
L represents the Liskov Substitution principle     -   build interchangeable parts, those must adhere to a contract
I represents the Interface Segregation principle   -   vegeterian adama et menyusunu verme
D represents the Dependency Inversion principle    -   Tv remote doesnt depend on brand name 
*/


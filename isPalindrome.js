//  Write a function that checks a string is a polindrome or not
// const isPalindrome = (string) => {
//     const reverseString = string.split('').reverse().join('');

//     if (string.toLowerCase() == reverseString.toLowerCase()) {
//         console.log('It is a palindrome');
//     }
//     else {
//         console.log('It is not a palindrome');
//     }
// }
// isPalindrome("Madam")

// function isPalindrome(str) {
//     const len = str.length;

//     for (let i = 0; i < len / 2; i++) {
//         if (str[i].toLowerCase() !== str[len - 1 - i].toLowerCase()) {
//             return 'It is not a palindrome';
//         }
//     }
//     return 'It is a palindrome';
// }


function isPalindrome(str) {
    console.log(str)
    if (str.length === 1) return true
    if (str[0] !== str[str.length - 1]) return false

    return isPalindrome(str.slice(1, str.length - 1))
}

console.log(isPalindrome("malayalam"))
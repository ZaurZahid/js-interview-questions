import React from 'react';
import classnames from 'classnames';
import lodash from 'lodash';
import axios from 'axios';

const ITEMS_API_URL = 'https://example.com/api/items';
const DEBOUNCE_DELAY = 500;

export default function Autocomplete() {
    const [items, setItems] = React.useState([]);
    const [query, setQuery] = React.useState('')
    const [loading, setLoading] = React.useState(false)

    React.useEffect(() => {
        getSuggestions('')
    }, [])

    const getSuggestions = async (word) => {
        if (word.trim().length) {
            setLoading(true);

            let response = await loadItems(word.trim());
            setItems(response);

            setLoading(false);
        } else {
            setItems([]);
        }
    };

    const loadItems = async (word) => {
        let result = axios
            .get(`https://api.publicapis.org/entries?title=${word}`)
            .then((response) => {
                return response.data.entries;
            })
            .catch((error) => {
                return error;
            });

        return result;
    }

    const runAfterFinishTyping = React.useCallback(
        lodash.debounce((newVal) => {
            getSuggestions(newVal)
        }, DEBOUNCE_DELAY),
        []
    );

    const handleSearch = (e) => {
        let value = e.target.value && e.target.value.toLowerCase()
        setQuery(value)

        runAfterFinishTyping(value);
    }

    return (
        <div className="wrapper">
            <div className={classnames("control")} >
                <input type="text" className="input" value={loading ? "loading..." : query} onChange={handleSearch} style={{ border: loading ? "2px solid blue" : undefined }} />
            </div>

            {items && items.length > 0 && !loading
                ? <ul className="list">
                    {items.map((item, index) =>
                        <li className="list-item" key={index} /* onClick={() => onSelectItem(item)} */>
                            {item.Description}
                        </li>
                    )}
                </ul>
                : null
            }

            <div className="list is-hoverable" />
        </div >
    );
}

//promiselere bax (nece promise yaradilir, nece resolve edirsen)
//https://medium.com/swlh/promises-in-javascript-c9764cba4a23

// let promiseA = Promise.resolve("A");
// let promiseB = Promise.resolve("B");
// let promiseC = new Promise(function (resolve, reject) {
//     setTimeout(resolve, 1000, "C");
// });

// Promise.all([promiseA, promiseB, promiseC])
//     .then(([resultA, resultB, resultC]) => {
//         console.log("result for promise " + resultA);
//         console.log("result for promise " + resultB);
//         console.log("result for promise " + resultC);
//     })
//     .catch((err) => {
//         console.log("Atleast one promise failed.");
//     });


// let promiseA = new Promise(function (resolve, reject) {
//     setTimeout(resolve, 400, "A");
// });
// let promiseB = new Promise(function (resolve, reject) {
//     setTimeout(resolve, 300, "B");
// });
// Promise.race([promiseA, promiseB])
//     .then((value) => console.log("Promise " + value + " is resolved"))
//     .catch((err) => console.log("Promise " + err + " is rejected"));

let chainA = function () {
    return new Promise((resolve, reject) => {
        setTimeout(function () {
            resolve("chainA");
        }, 1000);
    });
};
let chainB = function () {
    return new Promise((resolve, reject) => {
        setTimeout(function () {
            resolve("chainB");
        }, 3000);
    });
};
let chainC = function () {
    return Promise.reject("Error in chainC");
};
chainA()
    .then((data) => {
        console.log(data);
        return chainB();
    })
    .then((data) => {
        console.log(data);
        return chainC();
    })
    .then((data) => {
        console.log(data);
        console.log("all async request were successful");
    })
    .catch((err) => {
        console.log("common error handle");
        console.log("all async request were not successful with error " + err);
    });

// let promise = new Promise(function (resolve, reject) {
//     throw new Error("calculation error"); // throw will reject promise
//     reject("Reject!");
// });

// promise
//     .then(function (value) {
//         console.log(value);
//     })
//     .catch(function (err) {
//         console.log(err);
//     });